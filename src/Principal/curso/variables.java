package Principal.curso; //paquete 

//Nombre de la clase la cual debe de ser del mismo nombre del fichero .java
public class variables { 
	
	public String data() {
		// Variables
		/*	Es la forma de almacenar un dato en un programa 
		 * 	en la memoria del ordenador en binarios. */
	
	/****TIPOS DE DATOS****/
	/**Primitivos**/
	
		boolean VariableBoolean = true; // solo true o false	
		
		char VariableChar = 'A'; // caracteres unicodes
		
		byte VariableByte = 127; // entero de 8 bits
		
		short VariableShort = 1000; // entero de 16 bits
		
		int VariableInt = 1000000; // entero de 34 bits
		
		long VariableLong = 1234_5678_9012_3456L; // entero de 64 bits
		
		float VariableFloat = 37889f; // real de 32 bits
		
		double VariableDouble = 7890999.901; // real de 64 bits
		
		/*--------------------------------------------------------------------------------
		 * 
		Operador Uso Equivalente a 
		+= 
		op1 += op2 op1 = op1 + op2 
		-= 
		op1 -= op2 op1 = op1 - op2 
		*= 
		op1 *= op2 op1 = op1 * op2 
		/= 
		op1 /= op2 op1 = op1 / op2 
		%= 
		op1 %= op2 op1 = op1 % op2 
		*/
		
		return  "Boolean: "+VariableBoolean+"\n"+
				"Char: "+VariableChar +"\n"+
				"Byte: "+VariableByte +"\n"+
				"Short: "+VariableShort +"\n"+
				"Int: "+VariableInt +"\n"+
				"Long: "+VariableLong +"\n"+
				"Float: "+VariableFloat +"\n"+
				"Double: "+VariableDouble;
	
	}
	

	
}
